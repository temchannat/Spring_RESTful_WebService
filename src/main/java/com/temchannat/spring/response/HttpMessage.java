package com.temchannat.spring.response;

/**
 * Created by temchannat on 7/6/17.
 */
public class HttpMessage {

    /**
     * Response message with the successfuly transaction
     *
     * @param objectName
     * @param transaction
     * @return
     */
    public static String success(String objectName, String transaction) {
        return objectName + "  has been " + transaction +" successfully.";
    }


    /**
     * Response message with the fail transaction
     *
     * @param objectName
     * @param transaction
     * @return
     */
    public static String fail(String objectName, String transaction) {
        return objectName + "  could not " + transaction +".";
    }


    /**
     * Response message with invalid data.
     *
     * @param objectName
     * @param transaction
     * @param invalidMessage
     * @return
     */
    public static String invalid(String objectName, String transaction, String invalidMessage) {
        return objectName + " could not " + transaction + ". " + invalidMessage;
    }

}
