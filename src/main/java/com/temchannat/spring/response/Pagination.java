package com.temchannat.spring.response;

import com.sun.tools.internal.ws.processor.generator.CustomExceptionGenerator;

import java.io.Serializable;

/**
 * Created by temchannat on 7/5/17.
 */
public class Pagination implements Serializable {

    private static final long serialVersionUID = 1L;

    private int page;

    private int limit;

    private int totalCount;

    private int totalPages;


    public Pagination() {
        this(1, 15, 0, 0);
    }

    public Pagination(int page, int limit) {
        this.page = page;
        this.limit = limit;
    }

    public Pagination(int page, int limit, int totalCount, int totalPages) {
        this.page = page;
        this.limit = limit;
        this.totalCount = totalCount;
        this.totalPages = totalPages;
    }

    public int getPage() {
        return page;
    }

    public int totalPages() {
        return (int) Math.ceil((double) this.totalCount/ limit);
    }

    public int nextPage() {
        return this.page + 1;
    }

    public int previousPage() {
        return this.page - 1;
    }

    public boolean hasNextPage() {
        return this.nextPage() <= this.totalPages()? true : false;
    }

    public boolean hasPreviousPage() {
        return this.previousPage()>=1? true : false;
    }

    public int offset() {
        return (this.page-1) * limit;
    }

    public void setPage(int currentPage) {
        this.page = currentPage;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit() {
        this.limit = limit;
    }
//
//    public void setTotalCount(int totalCount) {
//        this.totalCount = totalCount;
//        this.totalPages = totalPages();
//        if(this.totalPages() < this.page){
//            throw new CustomExceptionGenerator("500", "The total pages has only " + this.totalPages() +" and your current page is "+ this.page);
//        }
//    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }





}
