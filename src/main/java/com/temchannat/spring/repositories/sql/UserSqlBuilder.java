package com.temchannat.spring.repositories.sql;

import com.temchannat.spring.entities.filters.UserFilter;
import com.temchannat.spring.entities.forms.UserForm;
import org.apache.ibatis.annotations.Param;

/**
 * Created by temchannat on 7/6/17.
 */
public class UserSqlBuilder {

    /**
     * Filter User
     * @param userFilter
     * @return
     */
    public static String myUserFilter(@Param("userFilter")UserFilter userFilter) {

        StringBuffer sqlBuffer = new StringBuffer();

        sqlBuffer.append("SELECT id, username, email, dob, gender, created_date, status, uuid " +
                "FROM users");

        // TODO: check what information is user privide to get FILTER
        if(userFilter.getUsername() == null && userFilter.getEmail() == null && userFilter.getStatus() == null &&
                (userFilter.getCreatedDate() == null || userFilter.getToCreatedDate() == null)) {
            return "";
        } else {
            boolean checkMore = false;
            sqlBuffer.append(" WHERE ");

            if(userFilter.getUsername() != null) {
                sqlBuffer.append("username LIKE '%' || #{userFilter.username} || '%' ");
                checkMore = true;
            }

            if(userFilter.getEmail() != null) {
                if(checkMore == true) {
                    sqlBuffer.append(" OR ");
                }
                sqlBuffer.append(" email LIKE '%' || #{userFilter.username} || '%' ");
            }

            if(userFilter.getStatus() != null) {
                if(checkMore == true) {
                    sqlBuffer.append(" OR ");
                }
                sqlBuffer.append(" status LIKE '%' || #{userFilter.status} || '%' ");
            }

            if(userFilter.getCreatedDate() != null && userFilter.getToCreatedDate() != null) {
                if(checkMore == true) {
                    sqlBuffer.append(" OR ");
                }
                sqlBuffer.append(" created_date >= #{userFilter.createdDate} " +
                        "AND created_date <= #{userFilter.toCreatedDate}");
            }

        }
        return sqlBuffer.toString();
    }

    /**
     * Search User
     * @param uuid
     * @return
     */
    public static String findUserByUUID(@Param("uuid") String uuid) {
        StringBuffer sqlBuffer = new StringBuffer();
        sqlBuffer.append("SELECT U.id, U.username, U.email, U.dob, U.gender, U.created_date, U.status, U.uuid " +
                "FROM users U WHERE U.uuid = #{uuid}");
        return sqlBuffer.toString();
    }

    /*
    public static String updateUser(@Param("userForm") UserForm userForm) {
        StringBuffer sqlUserForm = new StringBuffer();
        sqlUserForm.append("UPDATE users SET " +
                "username = #{userForm.username}, " +
                "email =  #{userForm.email}, " +
                "password = #{userForm.password}, " +
                "dob = #{userForm.dob}, " +
                "gender = #{userForm.gender}, " +
                "device = #{userForm.device}, " +
                "remark = #{userForm.remark}, " +
                "status = #{userForm.status} " +
                "WHERE uuid = #{userForm.uuid}");
        return sqlUserForm.toString();
    }
    */

}
