package com.temchannat.spring.repositories;

import com.temchannat.spring.entities.User;
import com.temchannat.spring.entities.filters.UserFilter;
import com.temchannat.spring.entities.forms.UserForm;
import com.temchannat.spring.repositories.sql.UserSqlBuilder;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by temchannat on 7/6/17.
 */

@Repository
public interface UserRepository {


    /**
     * Get All User
     *
     * @return
     */
    @Select("SELECT U.id, U.username, U.email, U.dob, U.gender, " +
            "U.created_date, U.status, U.uuid " +
            "FROM users U WHERE u.status = '1' ")
    @Results(value = {
            @Result(column = "created_date", property = "createdDate"),
            @Result(column = "id", property = "id"),
            @Result(column = "id", property = "roles",
                    many = @Many(select = "findRolesByUserId"))
    })
    List<UserForm> findAllUser();


    /**
     * Get RoleByUserId
     *
     * @param userId
     * @return
     */
    @Select("SELECT R.id, R.name, R.created_date, R.index " +
            "FROM roles R " +
            "INNER JOIN user_roles UR ON R.id = UR.role_id " +
            "WHERE UR.user_id = #{user_id}")
    @Results(value = {
            @Result(column = "created_date", property = "createdDate")
    })
    List<User> findRolesByUserId(@Param("user_id") int userId);

    /**
     * Get User by UUID
     * @param uuid
     * @return
     */


   @SelectProvider(type = UserSqlBuilder.class, method = "findUserByUUID")
   @Results(value = {
           @Result(column = "created_date", property = "createdDate"),
           @Result(column = "id", property = "roles",
                many = @Many(select = "findRolesByUserId"))
   })
    User findUserByUUID(@Param("uuid") String uuid);


    /**
     * Search User
     *
     * @param userFilter
     * @return
     */
    @SelectProvider(type = UserSqlBuilder.class, method = "myUserFilter")
    @Results(value = {
            @Result(column = "created_date", property = "createdDate"),
            @Result(column = "id", property = "roles",
                many = @Many(select = "findRolesByUserId"))
    })
    List<User> userFilter(@Param("userFilter")UserFilter userFilter);

    @Update("UPDATE users SET status = #{status} WHERE uuid = #{uuid} ")
    boolean updateUserStatus(@Param("uuid") String uuid, @Param("status") String status);

    @Update("UPDATE users SET status = '0' WHERE uuid = #{uuid}")
    boolean deleteUserByUUID(@Param("uuid") String uuid);

    //@SelectProvider(type = UserSqlBuilder.class, method = "updateUser")

    @Update("UPDATE users SET " +
            "username = #{userForm.username}, " +
            "email =  #{userForm.email}, " +
            "password = #{userForm.password}, " +
            "dob = #{userForm.dob}, " +
            "gender = #{userForm.gender}, " +
            "device = #{userForm.device}, " +
            "remark = #{userForm.remark}, " +
            "status = #{userForm.status} " +
            "WHERE uuid = #{userForm.uuid}")
    @SelectKey(
            statement = "SELECT id FROM users WHERE uuid = #{userForm.uuid}",
            keyProperty = "userForm.id",
            keyColumn = "id",
            before = false,
            resultType = int.class
    )
    boolean updateUser(@Param("userForm") UserForm userForm);

    @Insert("INSERT INTO users(uuid, username, email, password, dob, gender,  device, remark, status) " +
            "VALUES (" +
            "#{userForm.uuid}, #{userForm.username}, #{userForm.email}, #{userForm.password}, #{userForm.dob}, " +
            "#{userForm.gender}, #{userForm.device}, #{userForm.remark}, #{userForm.status}" +
            ");")
    @SelectKey(
            statement = "SELECT last_value FROM users_id_seq",
            keyProperty = "userForm.id",
            keyColumn = "last_value",
            before = false,
            resultType = int.class
    )
    boolean addUser(@Param("userForm") UserForm userForm);


    /**
     * Count total users
     *
     * @return
     */
    @Select("SELECT COUNT(*) as total_user FROM users WHERE status = '1'")
    int countTotalUsers();

    /**
     * Count Total Male
     *
     * @return
     */
    @Select("SELECT COUNT(gender) FROM users WHERE gender = 'M' AND status = '1'")
    int countMale();

    /**
     * Count total Female
     *
     * @return
     */
    @Select("SELECT COUNT(gender) FROM users WHERE gender = 'F' AND status = '1'")
    int countFemale();


}
