package com.temchannat.spring.repositories;

import com.temchannat.spring.entities.Role;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by temchannat on 7/8/17.
 */

@Repository
public interface RoleRepository {

    @Update("<script>" +
                "INSERT INTO user_roles VALUES " +
                    "<foreach collection='roleList' item='role' separator=','>" +
                        "(#{userId}, #{role.id})" +
                    "</foreach>" +
            "</script>")
    boolean addRoleByUserId(@Param("userId") int userId, @Param("roleList")List<Role> roles);



}
