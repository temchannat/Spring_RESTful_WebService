package com.temchannat.spring.services;

import com.temchannat.spring.entities.User;
import com.temchannat.spring.entities.filters.UserFilter;
import com.temchannat.spring.entities.forms.UserForm;

import java.util.List;

/**
 * Created by temchannat on 7/6/17.
 */
public interface UserService {

    List<UserForm> findAllUser();

    List<User> userFilter(UserFilter userFilter);

    User findUserByUUID(String uuid);

    boolean updateUserStatus(String uuid, String status);

    boolean deleteUserByUUID(String uuid);

    boolean updateUser(UserForm userForm);

    User addUser(UserForm userForm);




    int countTotalUsers();

    int countMale();

    int countFemale();



}
