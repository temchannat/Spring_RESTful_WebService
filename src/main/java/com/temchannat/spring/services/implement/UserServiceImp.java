package com.temchannat.spring.services.implement;

import com.temchannat.spring.entities.Role;
import com.temchannat.spring.entities.User;
import com.temchannat.spring.entities.filters.UserFilter;
import com.temchannat.spring.entities.forms.UserForm;
import com.temchannat.spring.repositories.RoleRepository;
import com.temchannat.spring.repositories.UserRepository;
import com.temchannat.spring.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by temchannat on 7/6/17.
 */

@Service
public class UserServiceImp implements UserService {

    UserRepository userRepository;
    RoleRepository roleRepository;

    @Autowired
    public UserServiceImp(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public List<UserForm> findAllUser() {
        return userRepository.findAllUser();
    }

    @Override
    public List<User> userFilter(UserFilter userFilter) {
        return userRepository.userFilter(userFilter);
    }

    @Override
    public User findUserByUUID(String uuid) {
        return userRepository.findUserByUUID(uuid);
    }

    @Override
    public boolean updateUserStatus(String uuid, String status) {
        return userRepository.updateUserStatus(uuid, status);
    }

    @Override
    public boolean deleteUserByUUID(String uuid) {
        return userRepository.deleteUserByUUID(uuid);
    }

    @Override
    public boolean updateUser(UserForm userForm) {
        return userRepository.updateUser(userForm);
    }

    @Override
    public User addUser(UserForm userForm) {
        userForm.setUuid(UUID.randomUUID().toString());
        if (userRepository.addUser(userForm)) {
            if (roleRepository.addRoleByUserId(userForm.getId(), userForm.getRoles())) {
                return userRepository.findUserByUUID(userForm.getUuid());
            }
        }
        return null;
    }

    @Override
    public int countTotalUsers() {
        return userRepository.countTotalUsers();
    }

    @Override
    public int countMale() {
        return userRepository.countMale();
    }

    @Override
    public int countFemale() {
        return userRepository.countFemale();
    }

}
