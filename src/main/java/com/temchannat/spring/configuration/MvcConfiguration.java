package com.temchannat.spring.configuration;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import java.util.Locale;

/**
 * Created by temchannat on 7/6/17.
 */

@Configuration
//@EnableWebMvc
public class MvcConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        super.addViewControllers(registry);

        registry.addViewController("/role-cu").setViewName("/role-cu");
        registry.addViewController("/role-list").setViewName("/role-list");
        registry.addViewController("/user-list").setViewName("/user-list");

        // Swagger
        registry.addViewController("/swagger/").setViewName("/home");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        super.addResourceHandlers(registry);
		/*
		 * Static Resources store in the project
		 */
        registry.addResourceHandler("/resources/**")
                .addResourceLocations("classpath:/static/");
		/*
		 * Static Resources store outside the project
		 */
        registry.addResourceHandler("/files/**")
                .addResourceLocations("file:/opt/FILES_MANAGEMENT/images/");



    }


    /**
     * SETUP languages
     *
     * Step 1
     *
     * @return
     */
    @Bean
    public LocaleResolver localeResolver() {
        CookieLocaleResolver resolver = new CookieLocaleResolver();
        resolver.setDefaultLocale(new Locale("kh"));
        resolver.setCookieName("Spring_Locale");
        resolver.setCookieMaxAge(4800);
        return resolver;
    }

    /**
     * SETUP languages
     *
     * Step 2:  to set parameter e.g ?lang=kh
     *
     * @return
     */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    /**
     * SETUP languages
     *
     * Step 3 to call localeChangeInterceptor to work
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
        registry.addInterceptor(localeChangeInterceptor());

    }

    /**
     * SETUP languages
     *
     * Step 4
     *
     * @return
     */
    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:/i18n/messages/message");
        messageSource.setCacheSeconds(0);
        messageSource.setUseCodeAsDefaultMessage(true);
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }





}
