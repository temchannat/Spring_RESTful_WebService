package com.temchannat.spring.controller;

import com.temchannat.spring.entities.User;
import com.temchannat.spring.entities.forms.UserForm;
import com.temchannat.spring.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by temchannat on 6/15/17.
 */

@Controller
public class MainController {

    List<UserForm> users = new ArrayList<>();


    UserService userService;

    @Autowired
    public MainController(UserService userService) {
        this.userService = userService;
    }


    /**
     * Dashboard or home page
     * Display Total of Users, Total of Male, Total of Female
     *
     * @param modelMap
     * @return
     */
    @RequestMapping({"/dashboard", "/"})
    public String dashboardPage(ModelMap modelMap) {
        int totalUsers = userService.countTotalUsers();
        int totalMale = userService.countMale();
        int totalFemale = userService.countFemale();

        modelMap.addAttribute("TOTALUSERS", totalUsers);
        modelMap.addAttribute("TOTALMALE", totalMale);
        modelMap.addAttribute("TOTALFEMALE", totalFemale);

        return "dashboard";
    }

    /**
     * List all users that status = 1
     *
     * @param modelMap
     * @return
     */
    @RequestMapping("/user-list")
    public String userList(ModelMap modelMap) {
        users = userService.findAllUser();
        modelMap.addAttribute("USERS", users);
        return "user-list";
    }

    /**
     * User form to input or update
     *
     * @param modelMap
     * @return
     */
    @RequestMapping("/user-cu")
    public String addUser(ModelMap modelMap) {
        modelMap.addAttribute("USERS", new UserForm());
        modelMap.addAttribute("saveStatus", true);
        return "user-cu";
    }

    /**
     * Save Users
     *
     * @param userForm
     * @param modelMap
     * @return
     */
    @PostMapping("/user-save")
    public String userSave(@ModelAttribute("USERS") UserForm userForm, ModelMap modelMap) {
        userService.addUser(userForm);
        modelMap.addAttribute("USERS", users);
        return "redirect:/user-list";

    }

    /**
     * Delete user, but it is just update users status to ZERO (0)
     *
     * @param uuid
     * @return
     */
    @RequestMapping("/user-delete/{uuid}")
    public String deleteUser(@PathVariable("uuid") String uuid) {
        userService.deleteUserByUUID(uuid);
        return "redirect:/user-list";
    }


    /**
     * When users click edit, it goes to this method first and then
     * redirect to /user-update
     *
     * @param uuid
     * @param modelMap
     * @return
     */
    @GetMapping("/user-edit/{uuid}")
    public String editUser(@PathVariable("uuid") String uuid, ModelMap modelMap) {
        User user = userService.findUserByUUID(uuid);
        modelMap.addAttribute("USERS", user);
        modelMap.addAttribute("saveStatus", false);
        return "user-cu";
    }

    /**
     * Update users and then redirect to /user-list
     *
     * @param user
     * @return
     */
//    @PostMapping("/user-update")
//    public String userUpdate(@ModelAttribute("user") User user) {
//        userService.updateByUserHash(user);
//        return "redirect:/user-list";
//    }


    /**
     * User Details
     *
     * @param uuid
     * @param model
     * @return
     */
    @RequestMapping("/user-detail/{uuid}")
    public String userDetails(@PathVariable("uuid") String uuid, ModelMap model) {
        User user = userService.findUserByUUID(uuid);
        model.addAttribute("USER", user);
        return "user-detail";
    }


}
