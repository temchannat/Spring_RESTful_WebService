package com.temchannat.spring.controller;

import com.temchannat.spring.entities.User;
import com.temchannat.spring.entities.filters.UserFilter;
import com.temchannat.spring.entities.forms.UserForm;
import com.temchannat.spring.response.*;
import com.temchannat.spring.response.failure.ResponseListFailure;
import com.temchannat.spring.response.failure.ResponseRecordFailure;
import com.temchannat.spring.services.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sun.tools.jconsole.Tab;

import javax.xml.crypto.Data;
import javax.xml.ws.Response;
import java.util.List;

/**
 * Created by temchannat on 7/6/17.
 */
@RestController
@RequestMapping("/v1/api/user")
public class UserRestController  {

    private UserService userService;
    private HttpStatus httpStatus = HttpStatus.OK;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/find-all-user")
    public ResponseEntity<ResponseList<UserForm>> findAllUser() {
        ResponseList<UserForm> responseList = new ResponseList<UserForm>();
        try{
            List<UserForm> users = userService.findAllUser();
            if(!users.isEmpty()) {
                responseList = new ResponseList<UserForm>(
                        HttpMessage.success(Table.USERS, Transaction.Success.RETRIEVE),
                        true,
                        users,
                        null);
            } else {
                 httpStatus = HttpStatus.NOT_FOUND;
                 responseList = new ResponseListFailure<UserForm>(
                         HttpMessage.fail(Table.USERS, Transaction.Fail.RETRIEVE),
                         false,
                         ResponseHttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            responseList = new ResponseListFailure<UserForm>(
                    HttpMessage.fail(Table.USERS, Transaction.Fail.RETRIEVE),
                    false,
                    ResponseHttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<ResponseList<UserForm>>(responseList, httpStatus);
    }

    @PostMapping("/filter")
    public ResponseEntity<ResponseList<User>> userFilter(@RequestBody UserFilter userFilter) {
        ResponseList<User> responseList = new ResponseList<>();
        try{
            List<User> users = userService.userFilter(userFilter);
            if(!users.isEmpty()) {
                responseList = new ResponseList<User>(
                        HttpMessage.success(Table.USERS, Transaction.Success.RETRIEVE),
                        true,
                        users,
                        null
                );
            } else {
                httpStatus = HttpStatus.NOT_FOUND;
                responseList = new ResponseListFailure<User>(
                        HttpMessage.fail(Table.USERS, Transaction.Fail.RETRIEVE),
                        false,
                        ResponseHttpStatus.NOT_FOUND
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            responseList = new ResponseListFailure<User>(
                    HttpMessage.fail(Table.USERS, Transaction.Fail.RETRIEVE),
                    false,
                    ResponseHttpStatus.INTERNAL_SERVER_ERROR
            );
        }
        return new ResponseEntity<ResponseList<User>>(responseList, httpStatus);
    }


    @GetMapping("/{uuid}")
    public ResponseEntity<ResponseRecord<User>> findUserByUUID(@PathVariable("uuid") String uuid) {
        ResponseRecord<User> responseList = new ResponseRecord<>();
        try{
            User users = userService.findUserByUUID(uuid);
            if(users != null) {
                responseList = new ResponseRecord<>(
                        HttpMessage.success(Table.USERS, Transaction.Success.RETRIEVE),
                        true,
                        users
                );
            } else {
                httpStatus = HttpStatus.NOT_FOUND;
                responseList = new ResponseRecordFailure<>(
                        HttpMessage.fail(Table.USERS, Transaction.Fail.RETRIEVE),
                        false,
                        ResponseHttpStatus.NOT_FOUND
                );
            }

        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            responseList = new ResponseRecordFailure<>(
                    HttpMessage.fail(Table.USERS, Transaction.Fail.RETRIEVE),
                    false,
                    ResponseHttpStatus.INTERNAL_SERVER_ERROR
            );
        }
        return new ResponseEntity<>(responseList, httpStatus);
    }

    @PutMapping("/status/{uuid}/{status}")
    public ResponseEntity<ResponseRecord> updateUserStatus(@PathVariable("uuid") String uuid, @PathVariable("status") String status) {
        ResponseRecord<User> responseRecord = new ResponseRecord<>();
        try{
            boolean updateStatusSuccessful = userService.updateUserStatus(uuid, status);
            if(updateStatusSuccessful) {
                responseRecord = new ResponseRecord<>(HttpMessage.success(Table.USERS, Transaction.Success.UPDATED),
                        true, null);

            } else {
                httpStatus = HttpStatus.NOT_FOUND;
                responseRecord = new ResponseRecordFailure<User>(
                        HttpMessage.fail(Table.USERS, Transaction.Fail.UPDATED),
                        false, ResponseHttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            responseRecord = new ResponseRecordFailure<User>(
                    HttpMessage.fail(Table.USERS, Transaction.Fail.UPDATED),
                    false, ResponseHttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<ResponseRecord>(responseRecord, httpStatus);
    }

    @PutMapping("/delete/{uuid}")
    public ResponseEntity<ResponseRecord> deleteUserByUUID(@PathVariable("uuid") String uuid) {
        ResponseRecord<User> responseRecord;
        try{
            boolean deleteUser = userService.deleteUserByUUID(uuid);
            if(deleteUser) {
                responseRecord = new ResponseRecord<>(HttpMessage.success(Table.USERS, Transaction.Success.DELETED),
                        true, null);
            } else {
                httpStatus = HttpStatus.NOT_FOUND;
                responseRecord = new ResponseRecordFailure<User>(
                        HttpMessage.fail(Table.USERS, Transaction.Fail.DELETED),
                        false, ResponseHttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            responseRecord = new ResponseRecordFailure<User>(
                    HttpMessage.fail(Table.USERS, Transaction.Fail.DELETED),
                    false, ResponseHttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<ResponseRecord>(responseRecord, httpStatus);
    }


    @PutMapping
    public ResponseEntity<ResponseRecord> updateUser(@RequestBody UserForm userForm) {
        ResponseRecord<User> responseRecord = new ResponseRecord<>();
        try{
            boolean updateSuccessful = userService.updateUser(userForm);
            if(updateSuccessful) {
                responseRecord = new ResponseRecord<>(HttpMessage.success(Table.USERS, Transaction.Success.UPDATED),
                        true, null);

            } else {
                httpStatus = HttpStatus.NOT_FOUND;
                responseRecord = new ResponseRecordFailure<User>(
                        HttpMessage.fail(Table.USERS, Transaction.Fail.UPDATED),
                        false, ResponseHttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            responseRecord = new ResponseRecordFailure<User>(
                    HttpMessage.fail(Table.USERS, Transaction.Fail.UPDATED),
                    false, ResponseHttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<ResponseRecord>(responseRecord, httpStatus);
    }

    @PostMapping
    public ResponseEntity<ResponseRecord<User>> addUser(@RequestBody UserForm userForm) {
        ResponseRecord<User> responseRecord  = new ResponseRecord<>();
        try{
            User addUser = userService.addUser(userForm);
            if(addUser != null) {
                responseRecord = new ResponseRecord<>(HttpMessage.success(Table.USERS, Transaction.Success.CREATED),
                        false,
                        addUser);
            } else {
                httpStatus = HttpStatus.BAD_REQUEST;
                responseRecord = new ResponseRecordFailure<>(
                        HttpMessage.fail(Table.USERS, Transaction.Fail.CREATED),
                        false,
                        ResponseHttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            responseRecord = new ResponseRecordFailure<>(
                    HttpMessage.fail(Table.USERS, Transaction.Fail.CREATED),
                    false,
                    ResponseHttpStatus.INTERNAL_SERVER_ERROR);

        }
        return new ResponseEntity<ResponseRecord<User>>(responseRecord, httpStatus);
    }


}
