package com.temchannat.spring.entities.forms;

import com.temchannat.spring.entities.Role;
import com.temchannat.spring.entities.User;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by temchannat on 7/7/17.
 */
public class UserForm {

    private int id;
    private String uuid;
    private String username;
    private String email;
    private String password;
    private Timestamp dob;
    private Timestamp createdDate;
    private String gender;
    private String device;
    private String remark;
    private String status;
    private List<Role> roles;

    public UserForm() {
    }

    public UserForm(int id, String uuid, String username, String email, String password, Timestamp dob, Timestamp createdDate, String gender, String device, String remark, String status, List<Role> roles) {
        this.id = id;
        this.uuid = uuid;
        this.username = username;
        this.email = email;
        this.password = password;
        this.dob = dob;
        this.createdDate = createdDate;
        this.gender = gender;
        this.device = device;
        this.remark = remark;
        this.status = status;
        this.roles = roles;
    }

    public UserForm(int id, String uuid, String username, String email, String password, Timestamp dob, String gender, String device, String remark, String status) {
        this.id = id;
        this.uuid = uuid;
        this.username = username;
        this.email = email;
        this.password = password;
        this.dob = dob;
        this.gender = gender;
        this.device = device;
        this.remark = remark;
        this.status = status;
    }

    public UserForm(int id, String uuid, String username, String email, String password, Timestamp dob, String gender, String device, String remark, String status, List<Role> roles) {
        this.id = id;
        this.uuid = uuid;
        this.username = username;
        this.email = email;
        this.password = password;
        this.dob = dob;
        this.gender = gender;
        this.device = device;
        this.remark = remark;
        this.status = status;
        this.roles = roles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getDob() {
        return dob;
    }

    public void setDob(Timestamp dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }
}
